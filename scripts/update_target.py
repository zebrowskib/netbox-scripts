from dcim.models import Device, DeviceRole, DeviceType, Manufacturer
from extras.scripts import *
from extras.models import Tag

class MultiTargetEdit(Script):
    class Meta:
        name = "Multi Target Edit"
        description = "Find and edit multiple targets at once"
        field_order = ['device_role','manufacturer','device_model','os_tags','target','tag_to_add']
        commit_default = False

    # get all the tags first with all their values
    tags_get = Tag.objects.values()
    # Take the results and create a list of dictionaries using only the slug and name fields
    temp_dict={'':'--------'} # create a default initial option
    for output in tags_get:
        temp_dict[output['slug']]=output['name']
    # Now convert that to a tuple so it can be used in the CHOICES section
    tag_list=[(k, v) for k, v in temp_dict.items()]

    # We build the choices out this way because when filtering target via tag, it wants it by slug instead of name or ID.
    CHOICES = (
        tag_list
    )
    device_role = ObjectVar(
        label='Device Type:',
        model=DeviceRole,
    default=4
    )
    manufacturer = MultiObjectVar(
        label='Device Manufacturer:',
        model=Manufacturer,
        display_field='Name',
        required=False
    )
    device_model = MultiObjectVar(
        label='Device Model:',
        model=DeviceType,
        display_field='model',
        required=False,
        query_params={
            'manufacturer_id': '$manufacturer'
        }
    )
    os_tags = ChoiceVar(
        choices=CHOICES,
        label='Device Tags:',
        required=False
    )
    target = MultiObjectVar(
        label='Target:',
        model=Device,
        display_field='Name',
        query_params={
            'manufacturer_id': '$manufacturer',
            'role_id': '$device_role',
        'tag': '$os_tags', #works BUT need the slug
        'device_type_id': '$device_model'
        }
    )
    tag_to_add = ChoiceVar(
        choices=CHOICES,
        label='Tag to Add:',
    description='Select the tag to be added to the selected targets above.'
    )

    def run(self, data, commit):
        tag_object = Tag.objects.get(slug=data['tag_to_add'])
        for devices in data['target']:
            device_object = Device.objects.get(name=f"{devices}")
            device_object.tags.add(f"{tag_object.name}")
            self.log_success(f"Updated device: {devices} with tag: {tag_object.name} which can be found in: {device_object.rack.name}, U{device_object.position}, {device_object.face.title()} side")

        global target_array
        global column_array
        target_array = {}
        column_array = ['Manufacturer','Model']

        for devices in data['target']:
            device_object = Device.objects.get(name=f"{devices}")
            target_array[device_object.name] = {'Manufacturer': device_object.device_type.manufacturer.name}
            target_array[device_object.name]['Model'] = device_object.device_type.model
            for line in device_object.comments.splitlines():
                if line.split(':')[0] not in column_array:
                    column_array.append(line.split(':')[0])
                if device_object.name in target_array:
                    target_array[device_object.name][line.split(':')[0]] = ':'.join(line.split(':')[1:]).lstrip()
                else:
                    target_array[device_object.name] = {line.split(':')[0]: ':'.join(line.split(':')[1:]).lstrip()}

        output = ['Name,'+','.join(column_array)]

        for devices in target_array:
            line = [devices]
            for column in column_array:
                # Try to print the column, but if its not set, then just print an empty space
                try:
                    line.append(target_array[devices][column])
                except(KeyError):
                    line.append('')
            output.append(','.join(line))

        return '\n'.join(output)

