from dcim.models import Device, DeviceType, Interface, Cable
from extras.scripts import *
from napalm import get_network_driver
import ipaddress, re

driver = get_network_driver('ios')
optional_args = {'force_no_enable': 'True'}

def get_cdp_neighbors(device):
    cdp = {}
    command = 'show cdp neighbors detail'
    output = device.cli([command])
    source_dev = device.get_facts()['hostname']
    d = []
    # Check if router supports the command
    if '% Invalid input' in output:
        return {}
    ref_id=0 # Need a unique way to create a listed dictionary since 1 switch can have multiple links to the same dest switch but on different ports
    d = {ref_id: {}}
    # Process the output to obtain just the CDP entries
    for line in output[command].split('\n'):
        if re.search('Device ID',line):
            ref_id+=1
            device_name = line.split(':')[1].split('.')[0].strip()
            d[ref_id-1] = {'source_dev': source_dev, 'dest_dev': device_name}
        if re.search('Interface',line):
            source_int = line.split(':')[1].split(',')[0].strip()
            dest_int = line.split(':')[2].split(',')[0].strip()
            d[ref_id-1]['source_int'] = source_int
            d[ref_id-1]['dest_int'] = dest_int
    return d

class UpdateCDPNeighbors(Script):
    class Meta:
        name = "Generate cable layout"
        description = "Create a csv with all cables connected to a switch based off of CDP neighbor information."
        field_order = ['username','password','device_model','target']
        commit_default = False

    username = StringVar(
        label='Username:',
        required='True'
    )

    password = StringVar(
        label='Password:',
        required='True'
    )

    device_model = MultiObjectVar(
        label='Device Model:',
        model=DeviceType,
        display_field='model',
        required=False,
        query_params={
            'manufacturer': 'cisco'
        }
    )

    target = MultiObjectVar(
        label='Target:',
        model=Device,
        display_field='Name',
        query_params={
            'platform': 'cisco-based',
            'device_type_id': '$device_model'
        }
    )

    def run(self, data, commit):
        output = ['side_a_device,side_a_type,side_a_name,side_b_device,side_b_type,side_b_name,type,status,color,label']
        for devices in data['target']:
            device_object = Device.objects.get(name=f"{devices}")
            device = driver(f"{ipaddress.IPv4Interface(devices.primary_ip4.address).ip}", f"{data['username']}", f"{data['password']}", optional_args=optional_args)
            device.open()
            cdp_dict = get_cdp_neighbors(device)
            device.close()
            for k,v in cdp_dict.items():
                if v['dest_dev']+",dcim.interface,"+v['dest_int']+","+v['source_dev']+",dcim.interface,"+v['source_int']+",mmf,connected,9c27b0,cdp_neighbor" in output:
                    pass
                else:
                    attrs = [v['source_dev'],'dcim.interface',v['source_int'],v['dest_dev'],'dcim.interface',v['dest_int'],'mmf','connected','9c27b0','cdp_neighbor']
                    output.append(','.join(attrs))
        return '\n'.join(output)
