from dcim.models import Device, DeviceType, Interface
from extras.scripts import *
from napalm import get_network_driver
import ipaddress, re

driver = get_network_driver('ios')
optional_args = {'force_no_enable': 'True'}

interface_types={
'FastEthernet': '100base-tx',
'GigabitEthernet': '1000base-t',
'TenGigabitEthernet': '10gbase-t',
'TwentyFiveGigE': '25gbase-x-sfp28',
'FortyGigabitEthernet': '40gbase-x-qsfpp',
'Port-channel': 'lag',
'Vlan': 'virtual',
'Embedded-Service-Engine': 'virtual',
'NVI': 'virtual'
}

class UpdateInterfaceInfo(Script):
    class Meta:
        name = "Update IOS device port information in netbox"
        description = "This will reach out to specified IOS devices and pull their current interface information and use that to update Netbox's information"
        field_order = ['username','password','device_model','target']
        commit_default = False

    username = StringVar(
        label='Username:',
        required='True'
    )
    password = StringVar(
        label='Password:',
        required='True'
    )

    device_model = MultiObjectVar(
        label='Device Model:',
        model=DeviceType,
        display_field='model',
        required=False,
        query_params={
            'manufacturer': 'cisco'
        }
    )
    target = MultiObjectVar(
        label='Target:',
        model=Device,
        display_field='Name',
        query_params={
            'platform': 'cisco-based',
            'device_type_id': '$device_model'
        }
    )

    def run(self, data, commit):
        for devices in data['target']:
            device_object = Device.objects.get(name=f"{devices}")
            device = driver(f"{ipaddress.IPv4Interface(devices.primary_ip4.address).ip}", f"{data['username']}", f"{data['password']}", optional_args=optional_args)
            device.open()
            actual_interfaces = device.get_interfaces()
            device.close()
            for k,v in actual_interfaces.items():
                try: # See if the interface already exists so that we know if we are updating or creating a new one and ONLY overwrite info that we can get from napalm, leave evertyhing else alone
                    update_interface = True
                    interface_object = Interface.objects.get(name=k,device_id=device_object.id)
                    interface_object.device = device_object
                    interface_object.name = k
                    interface_object.type = interface_types[re.split(r'\d',k)[0]]
                    interface_object.enabled = v['is_enabled']
                    interface_object.mac_address = v['mac_address']
                    interface_object.mtu = v['mtu']
                    interface_object.description = v['description']
                except:
                    update_interface = False
                    interface_object = Interface(
                        device = device_object,
                        name = k,
                        type = interface_types[re.split(r'\d',k)[0]],
                        enabled = v['is_enabled'],
                        mac_address = v['mac_address'],
                        mtu = v['mtu'],
                        description = v['description'],
                    )
                interface_object.save()
                if update_interface == True:
                    self.log_success(f"Updated interface {k} on {device_object.name} (object ID: {interface_object.id})")
                elif update_interface == False:
                    self.log_success(f"Created interface {k} on {device_object.name} (object ID: {interface_object.id})")
