#!/usr/bin/env python
import platform
from __future__ import print_function
import getpass, sys, argparse, re, os, time
from operator import itemgetter
import urllib3

try:
    import pyVmomi
    from pyVim import connect
    from pyVmomi import vim
    import pyVmomi
except:
    print("Run the following command first then retry: yum install python-pyvmomi -y")
    sys.exit(2)

from shop import vmware
urllib3.disable_warnings()

def writeToFile(fileName,stringToAdd,overwrite=False):
    if overwrite == False:
        f = open(fileName,'a')
    else:
        f = open(fileName,'w')
    f.write(stringToAdd)
    f.close()

# Used to get command line arguements for use in this script
usage = "usage: clone_template.py [-h] -u USER -s SERVER"
parser = OptionParser(usage=usage)
parser.add_option("-u", "--user", action="store", dest="user", help="[REQUIRED] Username to authenticate with")
parser.add_option("-s", "--server", action="store", dest="vcenter", help="[REQUIRED] vCenter server to connect to. Include full DNS name")
(options, args) = parser.parse_args()
# now define which options are required
if not options.user or not options.vcenter:
    print usage
    sys.exit(2)

password = getpass.getpass(prompt='Enter password for %s@%s: ' % (options.user, options.vcenter))

service_instance = None
try:
    if sys.version_info[0] == 2 and sys.version_info[1] <= 6: # Checking for 2.6 or below
        service_instance = connect.SmartConnect(host=options.vcenter, user=options.user, pwd=password)
    elif sys.version_info[0] == 2 and sys.version_info[1] >= 7: # Checking for 2.7 or above (2.7 enables   ssl verification, we turn it off)
        service_instance = connect.SmartConnectNoSSL(host=options.vcenter, user=options.user, pwd=password)
except IOError as e:
    pass

if not service_instance:
    raise SystemExit("Unable to connect to host with supplied info.")

# List of properties.
# http://vijava.sourceforge.net/vSphereAPIDoc/ver5/ReferenceGuide/vim.VirtualMachine.html
# https://www.vmware.com/support/developer/vc-sdk/visdk25pubs/ReferenceGuide/vim.vm.ConfigInfo.html
# for all properties.
vm_properties = ["name", "summary.config.template", "config.guestId", "config.hardware.memoryMB", "config.hardware.numCPU", "summary.storage.committed", "config.hardware.device"]

# Grab some initial data from vcenter
vm_data = ''
root_folder = service_instance.content.rootFolder
view = vmware.get_container_view(service_instance, obj_type=[vim.VirtualMachine])
vm_data = vmware.collect_properties(service_instance, view_ref=view, obj_type=vim.VirtualMachine, path_set=vm_properties, include_mors=True)

# Not sure how long this will be needed, but this will remove any unicode markings from within the vm_data dictionary
vm_data = vmware.convert(vm_data)

#content = service_instance.RetrieveContent()

# name and cluster are required by netbox, the rest is good to have. Status and tenant will probably be static
writeToFile('virtual_machine.csv',"name,status,vcpus,memory,disk\n",True)
writeToFile('virtual_machine_interfaces.csv',"virtual_machine,name,enabled,mac_address\n",True)
for vm_info in sorted(vm_data,key=itemgetter("name")):
    if vm_info['summary.config.template'] == False: #ignore templates
        total_hd = 0
        interface_macs = []
        for device in vm_info['config.hardware.device']:
            if 'Hard disk' in device.deviceInfo.label:
                # Going to add all of the HD's together to make 1 drive to be added to netbox.
                # WARNING: This does round DOWN, so if you have a HD of 128MB, it will be 0GB or 598.89GB, it will be 598GB. You have been warned!
                total_hd = total_hd + int(device.deviceInfo.summary.split(' ')[0].replace(',',''))/1024/1024
            if 'Network adapter' in device.deviceInfo.label:
                interface_macs.append(device.macAddress)
        writeToFile('virtual_machine.csv',vm_info['name']+',Active,'+str(vm_info['config.hardware.numCPU'])+','+str(vm_info['config.hardware.memoryMB'])+','+str(total_hd)+'\n')
        for list_index,list_item in enumerate(interface_macs):
            writeToFile('virtual_machine_interfaces.csv',vm_info['name']+',eth'+str(list_index)+',True,'+str(list_item)+'\n')
